package com.pbs.naresa;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import java.awt.Graphics;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

public class frmPrintCert extends JFrame implements Printable {

	private static final long serialVersionUID = 1L;
	
	private JPanel jContentPane = null;
	private JButton jButton = null;
	private JPanel lblBg = null;
	private Image bgimage = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/com/pbs/naresa/images/Naresa_pmtct_cert.jpg"));
	PrinterJob printJob = PrinterJob.getPrinterJob();
	PageFormat pf ;
	Paper a4paper;
	private JLabel lblDate = null;
	private Date siku1 = null;
	private JLabel lblName = null;
	private JLabel lblSerial = null;
	public static String certFname = "FirstName", certMname = "MiddleName", certLname="LastName", CertFinDate = "Today"; 
	public static long certSerial =  99999999;
	
	Format formater = new SimpleDateFormat("EEEE, dd MMMM yyyy");
	
	
	public void paint(Graphics g) {
       super.paint(g);
       //g.drawString("Hello world!", 35, 100);
    }

	
	/* (non-Javadoc)
	 * @see java.awt.print.Printable#print(java.awt.Graphics, java.awt.print.PageFormat, int)
	 */
	public int print(Graphics graphics, PageFormat pageFormat, int pageIndex)throws PrinterException {
		if (pageIndex > 0) {
            return Printable.NO_SUCH_PAGE;
        }
		pageFormat.setOrientation(PageFormat.LANDSCAPE);
        Graphics2D g2d = (Graphics2D)graphics;
        g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());        
        pageFormat.setOrientation(PageFormat.LANDSCAPE);        
        jContentPane.paint(g2d);
        
        return Printable.PAGE_EXISTS;
	}
	
	
	public void setPrintObj(){
		
		a4paper = new Paper();
		double paperWidth  =  8.26;
		//double paperHeight = 11.69;
		double paperHeight = 12.0;
		a4paper.setSize(paperWidth * 72.0, paperHeight * 72.0);
		
		double leftMargin   = 0.10; 
		double rightMargin  = 0.10;
		double topMargin    = 0.10;
		double bottomMargin = 0.10;

		a4paper.setImageableArea(leftMargin * 72.0, topMargin * 72.0,
                (paperWidth  - leftMargin - rightMargin)*72.0,
                (paperHeight - topMargin - bottomMargin)*72.0);

		
		
		
		pf = printJob.defaultPage();
		pf.setPaper(a4paper);
		pf.setOrientation(PageFormat.LANDSCAPE);
		printJob.setPrintable(this,pf);
		if (printJob.printDialog()) {
		      try {
		        printJob.print();
		      } catch (Exception PrintException) {
		        PrintException.printStackTrace();
		      }
		    }		
	}

	

	/**
	 * This is the default constructor
	 */
	public frmPrintCert() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		this.setName("frmprintcertificate");
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/com/pbs/naresa/images/logoNaresa.jpg")));
		this.setResizable(false);
		this.setBounds(new Rectangle(0, 0, 850, 662));
		this.setContentPane(getJContentPane());
		this.setTitle("Sample Certificate :: Ministry of Health (Naresa)");
		Main.centerWindow(this);	
		
		addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {            	
            	SwingUtilities.invokeLater(new Runnable() {
        			public void run() {
        				if(Main.frmcontent != null){
                    		Main.frmcontent.setVisible(true);
                            Main.frmprintcert.dispose();
                            Main.frmprintcert = null;
                        }else{
                            System.exit(1);
                        }
        			}
        			});
            	
            }
        });

		
	}
	
	
	
	
	
	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			lblSerial = new JLabel();
			lblSerial.setBounds(new Rectangle(600, 44, 193, 20));
			lblSerial.setHorizontalAlignment(SwingConstants.RIGHT);
			lblSerial.setHorizontalTextPosition(SwingConstants.RIGHT);
			lblSerial.setForeground(Color.blue);
			lblSerial.setText("Serial Number: "+System.currentTimeMillis());
			lblName = new JLabel();
			lblName.setBounds(new Rectangle(226, 301, 427, 27));
			lblName.setHorizontalAlignment(SwingConstants.CENTER);
			lblName.setHorizontalTextPosition(SwingConstants.CENTER);
			lblName.setFont(new Font("Old English Gothic", Font.BOLD, 36));
			lblName.setText("Peter Kenneth Marende");
			lblDate = new JLabel();
			lblDate.setBounds(new Rectangle(312, 404, 229, 23));
			lblDate.setHorizontalAlignment(SwingConstants.CENTER);
			lblDate.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 18));
			lblDate.setHorizontalTextPosition(SwingConstants.CENTER);
			Date siku = new Date();
			
			lblDate.setText(formater.format(siku));
			jContentPane = new JPanel();
			jContentPane.setLayout(null);
			jContentPane.setFont(new Font("Chianti BdIt Win95BT", Font.BOLD, 14));
			jContentPane.add(getJButton(), null);
			jContentPane.add(lblDate, null);
			jContentPane.add(lblName, null);
			jContentPane.add(lblSerial, null);
			jContentPane.add(getLblBg(), null);
			
			
			
			
		}
		return jContentPane;
	}

	/**
	 * This method initializes jButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButton() {
		if (jButton == null) {
			jButton = new JButton();
			jButton.setBounds(new Rectangle(348, 604, 129, 27));
			jButton.setText("Print");
			jButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					System.out.println("actionPerformed()");
					setPrintObj();
				}
			});
		}
		return jButton;
	}


	/**
	 * This method initializes lblBg	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getLblBg() {
		if (lblBg == null) {
			lblBg = new JPanel(){
				public void paint(Graphics g){
					g.drawImage(bgimage, 0, 0, this);
				}
			};
			lblBg.setLayout(new GridBagLayout());
			lblBg.setBounds(new Rectangle(0, 0, 850, 662));
			lblBg.setOpaque(false);
		}
		return lblBg;
	}


	/**
	 * This method initializes siku1	
	 * 	
	 * @return java.util.Date	
	 */
	private Date getSiku1() {
		if (siku1 == null) {
			siku1 = new Date();
		}
		return siku1;
	}
	
	

}  
