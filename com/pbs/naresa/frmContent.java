/**
 * 
 */
package com.pbs.naresa;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.event.HyperlinkEvent;

/**
 * @author Administrator
 *
 */
public class frmContent extends JFrame {

	private static final long serialVersionUID = 1L;
	
	protected Border txtborder = BorderFactory.createLineBorder(new Color(157,196,229));
	protected final Image frmContentBgImage =Toolkit.getDefaultToolkit().getImage(getClass().getResource("/com/pbs/naresa/images/concept_module_2.jpg"));  //  @jve:decl-index=0:
		
	private JPanel jContentPane = null;
	private JTabbedPane jtpContent = null;
	private JPanel jmod1 = null;
	private JPanel jmod2 = null;
	private JPanel jmod3 = null;
	private JPanel jmod4 = null;
	private JPanel jmod5 = null;
	private JPanel jmod6 = null;
	private JButton btnIntroduction = null;
	private JButton btnAppendix = null;
	private JButton btnResources = null;
	private JButton btnSkillBuilding = null;
	private JButton btnFaqs = null;
	private JButton btnExam = null;
	private JButton btnVideoClips = null;
	private JPanel jPanel6 = null;
	

	private JScrollPane jScrollPane = null;
	private JTextPane jtp1 = null;
	private JScrollPane jScrollPane1 = null;
	private JScrollPane jScrollPane2 = null;
	private JScrollPane jScrollPane3 = null;
	private JScrollPane jScrollPane4 = null;
	private JScrollPane jScrollPane5 = null;
	private JTextPane jtp2 = null;
	private JTextPane jtp3 = null;
	private JTextPane jtp4 = null;
	private JTextPane jtp5 = null;
	private JTextPane jtp6 = null;
	private JPanel pnlGeneral = null;
	private JScrollPane jspGeneral = null;
	private JEditorPane jepGen = null;
	private JPanel jPanel = null;
	
	
	private JButton btnSmpCert = null;
	private JButton btnVideo1 = null;
	private JButton btnVideo2 = null;
	private JButton btnVideo3 = null;
	private JButton btnVideo4 = null;
	private JButton btnVideo5 = null;
	private JButton btnVideo6 = null;
	
	 File videoFile1 = new File("movies/vidoe1.mpg");
	 File videoFile2 = new File("movies/vidoe2.mpg");
	 File videoFile3 = new File("movies/vidoe3.mpg");
	 File videoFile4 = new File("movies/vidoe4.mpg");
	 File videoFile5 = new File("movies/vidoe5.mpg");
	 File videoFile6 = new File("movies/vidoe6.mpg");
	
	
	/**
	 * This is the default constructor
	 */
	public frmContent() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setContentPane(getJContentPane());
		this.setTitle("Naresa contents  :: Ministry of Health (Naresa)");
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/com/pbs/naresa/images/logoNaresa.jpg")));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setBounds(new Rectangle(0, 0, 996, 726));
		this.btnIntroduction.doClick();
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(null);
			jContentPane.add(getJtpContent(), null);
			jContentPane.add(getBtnIntroduction(), null);
			jContentPane.add(getBtnAppendix(), null);
			jContentPane.add(getBtnResources(), null);
			jContentPane.add(getBtnSkillBuilding(), null);
			jContentPane.add(getBtnFaqs(), null);
			jContentPane.add(getBtnVideoClips(), null);
			jContentPane.add(getBtnExam(), null);
			jContentPane.add(getBtnSmpCert(), null);
			jContentPane.add(getJPanel6(), null);	
		}
		return jContentPane;
	}

	/**
	 * This method initializes jtpContent	
	 * 	
	 * @return javax.swing.JTabbedPane	
	 */
	private JTabbedPane getJtpContent() {
		if (jtpContent == null) {
			jtpContent = new JTabbedPane();
			jtpContent.setBounds(new Rectangle(223, 165, 754, 490));
			jtpContent.setForeground(new Color(72, 124, 138));
			jtpContent.addTab("General Module", null, getPnlGeneral(), null);
			jtpContent.addTab("Module 1", null, getJmod1(), null);
			jtpContent.addTab("Module 2", null, getJmod2(), null);
			jtpContent.addTab("Module 3", null, getJmod3(), null);
			jtpContent.addTab("Module 4", null, getJmod4(), null);
			jtpContent.addTab("Module 5", null, getJmod5(), null);
			jtpContent.addTab("Module 6", null, getJmod6(), null);
			jtpContent.addTab("Video Clips", null, getJPanel(), null);
			jtpContent.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					
					int jtp = jtpContent.getSelectedIndex();
										
					switch (jtp) {
					case 1:
						try{
							
							jtpContent.setSelectedIndex(1);
							URL myurl = getClass().getResource("/com/pbs/naresa/content/module1/module1-1.html");
							jtp1.setPage(myurl);
						}catch(Exception exc){exc.toString();}
					break;
					case 2:
						try{
							jtpContent.setSelectedIndex(2);
							URL myurl = getClass().getResource("/com/pbs/naresa/content/module2/module2-1.html");
							jtp2.setPage(myurl);
						}catch(Exception exc){exc.toString();}
					break;
					case 3:
						try{
							jtpContent.setSelectedIndex(3);							
							URL myurl = getClass().getResource("/com/pbs/naresa/content/module3/module3-1.html");
							jtp3.setPage(myurl);
						}catch(Exception exc){exc.toString();}
					break;
					case 4:
						try{
							jtpContent.setSelectedIndex(4);
							URL myurl = getClass().getResource("/com/pbs/naresa/content/module4/module4_1.html");
							jtp4.setPage(myurl);
						}catch(Exception exc){exc.toString();}
					break;
					case 5:
						try{
							jtpContent.setSelectedIndex(5);
							URL myurl = getClass().getResource("/com/pbs/naresa/content/module5/module5_1.html");
							jtp5.setPage(myurl);
						}catch(Exception exc){exc.toString();}
					break;
					case 6:
						try{
							jtpContent.setSelectedIndex(6);
							URL myurl = getClass().getResource("/com/pbs/naresa/content/module6/module6_1.html");
							jtp6.setPage(myurl);
						}catch(Exception exc){exc.toString();}
					break;
					default:
					//System.out.println("Consider cheating default!");
					}
					
				}
			});
			jtpContent.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
				public void propertyChange(java.beans.PropertyChangeEvent e) {
					if ((e.getPropertyName().equals("enabled"))) {
						System.out.println("propertyChange(enabled)");  
					}
				}
			});
			
		}
		return jtpContent;
	}

	/**
	 * This method initializes jmod1	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJmod1() {
		if (jmod1 == null) {
			jmod1 = new JPanel();
			jmod1.setLayout(null);
			jmod1.setBackground(Color.white);
			jmod1.add(getJScrollPane(), null);
		}
		return jmod1;
	}

	/**
	 * This method initializes jmod2	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJmod2() {
		if (jmod2 == null) {
			jmod2 = new JPanel();
			jmod2.setLayout(null);
			jmod2.add(getJScrollPane1(), null);
		}
		return jmod2;
	}

	/**
	 * This method initializes jmod3	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJmod3() {
		if (jmod3 == null) {
			jmod3 = new JPanel();
			jmod3.setLayout(null);
			jmod3.add(getJScrollPane2(), null);
		}
		return jmod3;
	}

	/**
	 * This method initializes jmod4	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJmod4() {
		if (jmod4 == null) {
			jmod4 = new JPanel();
			jmod4.setLayout(null);
			jmod4.add(getJScrollPane3(), null);
		}
		return jmod4;
	}

	/**
	 * This method initializes jmod5	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJmod5() {
		if (jmod5 == null) {
			jmod5 = new JPanel();
			jmod5.setLayout(null);
			jmod5.add(getJScrollPane4(), null);
		}
		return jmod5;
	}

	/**
	 * This method initializes jmod6	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJmod6() {
		if (jmod6 == null) {
			jmod6 = new JPanel();
			jmod6.setLayout(null);
			jmod6.add(getJScrollPane5(), null);
		}
		return jmod6;
	}

	/**
	 * This method initializes btnIntroduction	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnIntroduction() {
		if (btnIntroduction == null) {
			btnIntroduction = new JButton();
			btnIntroduction.setBounds(new Rectangle(0, 165, 206, 33));
			btnIntroduction.setText("Introduction");
			btnIntroduction.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					jtpContent.setSelectedIndex(0);
					try{
						URL myurl = getClass().getResource("/com/pbs/naresa/content/introductions/intro-1.html");
						jepGen.setPage(myurl);
					}catch(Exception exc){exc.toString();}
				}
			});
		}
		return btnIntroduction;
	}

	/**
	 * This method initializes btnAppendix	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnAppendix() {
		if (btnAppendix == null) {
			btnAppendix = new JButton();
			btnAppendix.setBounds(new Rectangle(0, 205, 206, 33));
			btnAppendix.setText("Appendix");
			btnAppendix.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					jtpContent.setSelectedIndex(0);
					try{
						jepGen.setPage(getClass().getResource("/com/pbs/naresa/content/Appendices/app-1.html"));
					}catch(Exception exc){exc.toString();}
				}
			});
		}
		return btnAppendix;
	}

	/**
	 * This method initializes btnResources	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnResources() {
		if (btnResources == null) {
			btnResources = new JButton();
			btnResources.setBounds(new Rectangle(0, 242, 206, 33));
			btnResources.setText("Resources");
			btnResources.addActionListener(new java.awt.event.ActionListener() {   
				public void actionPerformed(java.awt.event.ActionEvent e) {    
					jtpContent.setSelectedIndex(0);
					try{
						jepGen.setPage(getClass().getResource("/com/pbs/naresa/content/Resources/Resources -1.html"));
					}catch(Exception exc){exc.toString();}
				}
			
			});
		}
		return btnResources;
	}

	/**
	 * This method initializes btnSkillBuilding	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnSkillBuilding() {
		if (btnSkillBuilding == null) {
			btnSkillBuilding = new JButton();
			btnSkillBuilding.setBounds(new Rectangle(0, 280, 206, 33));
			btnSkillBuilding.setText("Skill Building");
			btnSkillBuilding.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					jtpContent.setSelectedIndex(0);
					try{
						jepGen.setPage(getClass().getResource("/com/pbs/naresa/content/skill-building/skill-1.html"));
					}catch(Exception exc){exc.toString();}
				}
			});
		}
		return btnSkillBuilding;
	}

	/**
	 * This method initializes btnFaqs	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnFaqs() {
		if (btnFaqs == null) {
			btnFaqs = new JButton();
			btnFaqs.setBounds(new Rectangle(0, 320, 206, 33));
			btnFaqs.setText("FAQS");
			btnFaqs.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					jtpContent.setSelectedIndex(0);
					try{
						jepGen.setPage(getClass().getResource("/com/pbs/naresa/content/faqs/faq-1.html"));
						}catch(Exception exc){exc.toString();}
				}
			});
		}
		return btnFaqs;
	}

	/**
	 * This method initializes btnVideoClips	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnVideoClips() {
		if (btnVideoClips == null) {
			btnVideoClips = new JButton();
			btnVideoClips.setBounds(new Rectangle(0, 360, 206, 33));
			btnVideoClips.setText("Video Clips");
			btnVideoClips.addActionListener(new java.awt.event.ActionListener() {   
				public void actionPerformed(java.awt.event.ActionEvent e) {
					jtpContent.setSelectedIndex(7);
				}
			
			});
		}
		return btnVideoClips;
	}

	/**
	 * This method initializes jPanel6	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel6() {
		if (jPanel6 == null) {
			jPanel6 = new JPanel(){
				public void paint(Graphics g){
					g.drawImage(frmContentBgImage, 0, 0, this);
				}
			};
			jPanel6.setLayout(new GridBagLayout());
			jPanel6.setBounds(new Rectangle(0, 0, 996, 706));
			jPanel6.setOpaque(false);
		}
		return jPanel6;
	}

	/**
	 * This method initializes jScrollPane	
	 * 	
	 * @return javax.swing.JScrollPane	
	 */
	private JScrollPane getJScrollPane() {
		if (jScrollPane == null) {
			jScrollPane = new JScrollPane();
			jScrollPane.setBounds(new Rectangle(0, 0, 750, 464));
			jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
			jScrollPane.setViewportView(getJtp1());
		}
		return jScrollPane;
	}

	/**
	 * This method initializes jtp1	
	 * 	
	 * @return javax.swing.JTextPane	
	 */
	private JTextPane getJtp1() {
		if (jtp1 == null) {
			jtp1 = new JTextPane();
			jtp1.setBounds(new Rectangle(0, 0, 740, 450));
			jtp1.setContentType("text/html");
			jtp1.setEditable(false);
			jtp1.setBackground(new Color(255, 231, 191));
			jtp1.setBorder(txtborder);
			jtp1.addHyperlinkListener(new javax.swing.event.HyperlinkListener() {
				public void hyperlinkUpdate(javax.swing.event.HyperlinkEvent e) {
					if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED)
						try {
							jtp1.setPage(e.getURL());
						} catch (IOException e1) {e1.printStackTrace();}
				}
			});
		}
		return jtp1;
	}

	/**
	 * This method initializes jScrollPane1	
	 * 	
	 * @return javax.swing.JScrollPane	
	 */
	private JScrollPane getJScrollPane1() {
		if (jScrollPane1 == null) {
			jScrollPane1 = new JScrollPane();
			jScrollPane1.setBounds(new Rectangle(0, 0, 749, 461));
			jScrollPane1.setViewportView(getJtp2());
		}
		return jScrollPane1;
	}

	/**
	 * This method initializes jScrollPane2	
	 * 	
	 * @return javax.swing.JScrollPane	
	 */
	private JScrollPane getJScrollPane2() {
		if (jScrollPane2 == null) {
			jScrollPane2 = new JScrollPane();
			jScrollPane2.setBounds(new Rectangle(0, 0, 749, 462));
			jScrollPane2.setViewportView(getJtp3());
		}
		return jScrollPane2;
	}

	/**
	 * This method initializes jScrollPane3	
	 * 	
	 * @return javax.swing.JScrollPane	
	 */
	private JScrollPane getJScrollPane3() {
		if (jScrollPane3 == null) {
			jScrollPane3 = new JScrollPane();
			jScrollPane3.setBounds(new Rectangle(0, 0, 748, 463));
			jScrollPane3.setViewportView(getJtp4());
		}
		return jScrollPane3;
	}

	/**
	 * This method initializes jScrollPane4	
	 * 	
	 * @return javax.swing.JScrollPane	
	 */
	private JScrollPane getJScrollPane4() {
		if (jScrollPane4 == null) {
			jScrollPane4 = new JScrollPane();
			jScrollPane4.setBounds(new Rectangle(0, 0, 749, 461));
			jScrollPane4.setViewportView(getJtp5());
		}
		return jScrollPane4;
	}

	/**
	 * This method initializes jScrollPane5	
	 * 	
	 * @return javax.swing.JScrollPane	
	 */
	private JScrollPane getJScrollPane5() {
		if (jScrollPane5 == null) {
			jScrollPane5 = new JScrollPane();
			jScrollPane5.setBounds(new Rectangle(0, 0, 748, 462));
			jScrollPane5.setViewportView(getJtp6());
		}
		return jScrollPane5;
	}

	/**
	 * This method initializes jtp2	
	 * 	
	 * @return javax.swing.JTextPane	
	 */
	private JTextPane getJtp2() {
		if (jtp2 == null) {
			jtp2 = new JTextPane();
			jtp2.setContentType("text/html");
			jtp2.setBackground(new Color(255, 231, 191));
			jtp2.setEditable(false);
			jtp2.addHyperlinkListener(new javax.swing.event.HyperlinkListener() {
				public void hyperlinkUpdate(javax.swing.event.HyperlinkEvent e) {
					if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED)
						try {
							jtp2.setPage(e.getURL());
						} catch (IOException e1) {e1.printStackTrace();	}
				}
			});
		}
		return jtp2;
	}

	/**
	 * This method initializes jtp3	
	 * 	
	 * @return javax.swing.JTextPane	
	 */
	private JTextPane getJtp3() {
		if (jtp3 == null) {
			jtp3 = new JTextPane();
			jtp3.setContentType("text/html");
			jtp3.setBackground(new Color(255, 231, 191));
			jtp3.setEditable(false);
			jtp3.addHyperlinkListener(new javax.swing.event.HyperlinkListener() {
				public void hyperlinkUpdate(javax.swing.event.HyperlinkEvent e) {
					if(e.getEventType()== HyperlinkEvent.EventType.ACTIVATED)
						try{
							jtp3.setPage(e.getURL());
						}catch (Exception ex){}
				}
			});
		}
		return jtp3;
	}

	/**
	 * This method initializes jtp4	
	 * 	
	 * @return javax.swing.JTextPane	
	 */
	private JTextPane getJtp4() {
		if (jtp4 == null) {
			jtp4 = new JTextPane();
			jtp4.setContentType("text/html");
			jtp4.setBackground(new Color(255, 231, 191));
			jtp4.setEditable(false);
			jtp4.addHyperlinkListener(new javax.swing.event.HyperlinkListener() {
				public void hyperlinkUpdate(javax.swing.event.HyperlinkEvent e) {
					if(e.getEventType()== HyperlinkEvent.EventType.ACTIVATED)
						try{
							jtp4.setPage(e.getURL());
						}catch (Exception ex){}
				}
			});
		}
		return jtp4;
	}

	/**
	 * This method initializes jtp5	
	 * 	
	 * @return javax.swing.JTextPane	
	 */
	private JTextPane getJtp5() {
		if (jtp5 == null) {
			jtp5 = new JTextPane();
			jtp5.setContentType("text/html");
			jtp5.setBackground(new Color(255, 231, 191));
			jtp5.setEditable(false);
			jtp5.addHyperlinkListener(new javax.swing.event.HyperlinkListener() {
				public void hyperlinkUpdate(javax.swing.event.HyperlinkEvent e) {
					if(e.getEventType()== HyperlinkEvent.EventType.ACTIVATED)
						try{
							jtp5.setPage(e.getURL());
						}catch (Exception ex){}
				}
			});
		}
		return jtp5;
	}

	/**
	 * This method initializes jtp6	
	 * 	
	 * @return javax.swing.JTextPane	
	 */
	private JTextPane getJtp6() {
		if (jtp6 == null) {
			jtp6 = new JTextPane();
			jtp6.setContentType("text/html");
			jtp6.setBackground(new Color(255, 231, 191));
			jtp6.setEditable(false);
			jtp6.addHyperlinkListener(new javax.swing.event.HyperlinkListener() {
				public void hyperlinkUpdate(javax.swing.event.HyperlinkEvent e) {
					System.out.println("hyperlinkUpdate()"); // TODO Auto-generated Event stub hyperlinkUpdate()
				}
			});
		}
		return jtp6;
	}

	/**
	 * This method initializes pnlGeneral	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getPnlGeneral() {
		if (pnlGeneral == null) {
			pnlGeneral = new JPanel();
			pnlGeneral.setLayout(null);
			pnlGeneral.add(getJspGeneral(), null);
		}
		return pnlGeneral;
	}

	/**
	 * This method initializes jspGeneral	
	 * 	
	 * @return javax.swing.JScrollPane	
	 */
	private JScrollPane getJspGeneral() {
		if (jspGeneral == null) {
			jspGeneral = new JScrollPane();
			jspGeneral.setBounds(new Rectangle(0, 0, 749, 462));
			jspGeneral.setViewportView(getJepGen());
			jspGeneral.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
			//jspGeneral.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		}
		return jspGeneral;
	}

	/**
	 * This method initializes jepGen	
	 * 	
	 * @return javax.swing.JEditorPane	
	 */
	private JEditorPane getJepGen() {
		if (jepGen == null) {
			jepGen = new JEditorPane();
			jepGen.setEditable(false);
			jepGen.setFont(new Font("Arial", Font.PLAIN, 12));
			jepGen.setBackground(new Color(255, 231, 191));
			jepGen.setContentType("text/html");
			jepGen.addHyperlinkListener(new javax.swing.event.HyperlinkListener() {
				public void hyperlinkUpdate(javax.swing.event.HyperlinkEvent e) {
					//System.out.println("url is: "+e.getURL());
					if(e.getEventType()== HyperlinkEvent.EventType.ACTIVATED)
						try{
							jepGen.setPage(e.getURL());
						}catch (Exception ex){}
				}
			});
		}
		return jepGen;
	}



	/**
	 * This method initializes jPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel() {
		if (jPanel == null) {
			jPanel = new JPanel();
			jPanel.setLayout(null);
			jPanel.setBackground(new Color(193, 218, 240));
			jPanel.add(getBtnVideo1(), null);
			jPanel.add(getBtnVideo2(), null);
			jPanel.add(getBtnVideo3(), null);
			jPanel.add(getBtnVideo4(), null);
			jPanel.add(getBtnVideo5(), null);
			jPanel.add(getBtnVideo6(), null);
		}
		return jPanel;
	}



	/**
	 * This method initializes btnExam	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnExam() {
		if (btnExam == null) {
			btnExam = new JButton();
			btnExam.setText("Take Exams");
			btnExam.setBounds(new Rectangle(0, 400, 206, 33));
			btnExam.addActionListener(new java.awt.event.ActionListener() {   
				public void actionPerformed(java.awt.event.ActionEvent e) {    			
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							if(Main.frmlogin == null){
								Main.frmlogin = new frmLogin();
								Main.frmlogin.setVisible(true);						
								Main.frmcontent.setVisible(false);
							}else{
								Main.frmlogin.setVisible(true);
								Main.frmcontent.setVisible(false);					
							}
						}
						});
				}
			
			});
		}
		return btnExam;
	}

	
	/**
	 * This method initializes btnSmpCert	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnSmpCert() {
		if (btnSmpCert == null) {
			btnSmpCert = new JButton();
			btnSmpCert.setBounds(new Rectangle(0, 440, 206, 33));
			btnSmpCert.setText("Sample Certificate");
			btnSmpCert.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					
					if(Main.frmprintcert == null){
						Main.frmprintcert = new frmPrintCert();
						Main.frmprintcert.setVisible(true);						
						Main.frmcontent.setVisible(false);
					}else{
						Main.frmprintcert.setVisible(true);
						Main.frmcontent.setVisible(false);
					}
				}
			});
		}
		return btnSmpCert;
	}



	/**
	 * This method initializes btnVideo1	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnVideo1() {
		if (btnVideo1 == null) {
			btnVideo1 = new JButton();
			btnVideo1.setBounds(new Rectangle(51, 35, 200, 85));
			btnVideo1.setText("Video One");
			btnVideo1.addActionListener(new java.awt.event.ActionListener() {   
				public void actionPerformed(java.awt.event.ActionEvent e) {    
				
				}
			
			});
		}
		return btnVideo1;
	}



	/**
	 * This method initializes btnVideo2	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnVideo2() {
		if (btnVideo2 == null) {
			btnVideo2 = new JButton();
			btnVideo2.setBounds(new Rectangle(480, 42, 196, 77));
			btnVideo2.setText("Video Two");
			btnVideo2.addActionListener(new java.awt.event.ActionListener() {   
				public void actionPerformed(java.awt.event.ActionEvent e) {    
					
				}
			
			});
		}
		return btnVideo2;
	}



	/**
	 * This method initializes btnVideo3	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnVideo3() {
		if (btnVideo3 == null) {
			btnVideo3 = new JButton();
			btnVideo3.setBounds(new Rectangle(263, 140, 211, 88));
			btnVideo3.setText("Video Three");
			btnVideo3.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {					
				
				}
			});
		}
		return btnVideo3;
	}



	/**
	 * This method initializes btnVideo4	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnVideo4() {
		if (btnVideo4 == null) {
			btnVideo4 = new JButton();
			btnVideo4.setBounds(new Rectangle(45, 254, 187, 102));
			btnVideo4.setText("Video Four");
			btnVideo4.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {

				}
			});
		}
		return btnVideo4;
	}



	/**
	 * This method initializes btnVideo5	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnVideo5() {
		if (btnVideo5 == null) {
			btnVideo5 = new JButton();
			btnVideo5.setBounds(new Rectangle(482, 258, 199, 88));
			btnVideo5.setText("Video Five");
			btnVideo5.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					
				}
			});
		}
		return btnVideo5;
	}



	/**
	 * This method initializes btnVideo6	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnVideo6() {
		if (btnVideo6 == null) {
			btnVideo6 = new JButton();
			btnVideo6.setBounds(new Rectangle(265, 362, 213, 80));
			btnVideo6.setText("Video Six");
			btnVideo6.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					
				}
			});
		}
		return btnVideo6;
	}



	public static void main(String[] args) {
		// TODO Auto-generated method stub
	}
	
}
