package com.pbs.naresa;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;


public class dbFunction {
	
	
	
	Connection con = null;
	DatabaseMetaData dbmd =null;
	Statement st = null;
	ResultSet stGans = null;
	ResultSet urs = null;

	 
		public void connection(String s){	
			try{	
				Class.forName("org.h2.Driver");
				con = DriverManager.getConnection("jdbc:h2:"+s+";IFEXISTS=TRUE", "sa", "");
				if(con != null)
				con.getMetaData();
				st = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);		
			}catch(Exception ex){System.out.println(ex.toString());}
				
		}//end of connection		



		public void closeConnection(){
			try {
				con.close();
				} catch (SQLException e) {e.printStackTrace();}
		}
		
		public void closeAllConnection(){
			
			try {
				if(st!= null){
					st.close();
					st = null;
				}
				if(urs!= null){
					urs.close();
					urs = null;
				}
				if(con != null){
					con.close();
					con = null;
				}
				
			} catch (SQLException e) {e.printStackTrace();}
		}
		
		protected int dbUpdate(String s){
		    int i =0;
		    try{
		            urs=null;
		            i = st.executeUpdate(s);
		        }catch(SQLException e) {System.out.println(e.toString());}
		      return i;
		}

}



