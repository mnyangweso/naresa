/**
 * 
 */
package com.pbs.naresa;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

/**
 * @author Administrator
 *
 */
public class moveDb {
	
	FileOutputStream out = null;
	String files[]={"naresa.221.log.db","naresa.data.db","naresa.index.db","naresa.trace.db"};
	InputStream is= null;
	String mvErr = null;
	String destiny = null;
	
	
	public moveDb(){
		doMoveFiles();
	}//end of constructor
	
	public void checkDir(String dir){
		String err = null;
		File chkdir = new File(dir);
		
		if(!chkdir.isDirectory())
			err = "You need to choose a directory";		
		if(!chkdir.canWrite())
			err += "You dont have write permisions to that directory";
		
		File dbdir = new File(dir+"/db");
		if(!dbdir.exists())
			err += "files exists";
		else{
			if(dbdir.mkdir())
				System.out.println("dir is succesfully created");
			else
				System.out.println("can't create directory");
			
		}
			
		
		
	}
	
	public void doMoveFiles(){
		for(int i=0;i<files.length;i++)
			moveFiles(files[i],destiny);
		if(mvErr == null)
			System.out.println("Files were copied succesfull");		
	}
	
	public void moveFiles(String fileToCopy,String destinationDir){
			try{				
				is = getClass().getResourceAsStream("/com/pbs/naresa/db/"+fileToCopy);
				if(is != null){
					out = new FileOutputStream(destinationDir+fileToCopy);
			        int c;
			        while ((c = is.read()) != -1) 
			            out.write(c);	
				}else{
					mvErr += fileToCopy+"\n";
					System.out.println("the file does not exists"+fileToCopy);
				}
			} catch (Exception e) { e.printStackTrace();}
			finally{
				try {
					out.close(); out=null;
					is.close();  is=null;
				} catch (IOException e) {e.printStackTrace();}
			}
	}
	
	

}
