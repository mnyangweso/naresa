/**
 * 
 */
package com.pbs.naresa;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.net.URL;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;

/**
 * @author Administrator
 *
 */
class Main {

	/**
	 * @param args
	 */
	protected static final double jdkVersion = 1.6;
	protected String jdk = System.getProperty("java.specification.version");
	protected Double jdkcomp = new Double(jdk);
	
	protected final Color foreColor = new Color(72, 124, 138);
	
	protected static String dbpath = null;
	
	protected final Border txtborder = BorderFactory.createLineBorder(new Color(157,196,229));
		
	protected static frmContent frmcontent = null;
	protected static frmPrintCert frmprintcert = null;
	protected static frmLogin frmlogin = null;
	
	
	public static void centerWindow(JFrame dis) {
        // Center the window
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension frameSize = dis.getSize();
        if (frameSize.height > screenSize.height) {
            frameSize.height = screenSize.height;
        }
        if (frameSize.width > screenSize.width) {
            frameSize.width = screenSize.width;
        }
        dis.setLocation((screenSize.width - frameSize.width) / 2,
                         (screenSize.height - frameSize.height) / 2);
        dis.setVisible(true);
    }
	
	protected Image getImage(String str){
        Image image = null;
        URL url = getClass().getResource(str);
        image =  Toolkit.getDefaultToolkit().getImage(url);
    return image;
    }
	
	
	public static void main(String[] args) {		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				frmcontent = new frmContent();
				centerWindow(frmcontent);
				frmcontent.setVisible(true);				
				frmcontent.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			}
			});
	}

	

}
