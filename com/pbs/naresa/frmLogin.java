/**
 * 
 */
package com.pbs.naresa;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Rectangle;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import java.awt.Toolkit;
import java.io.File;

import javax.swing.WindowConstants;
import javax.swing.border.Border;

/**
 * @author Administrator
 *
 */
public class frmLogin extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JButton btnAttach = null;
	private JButton btnReset = null;
	private JButton btnLogin = null;
	private JTextField txtName = null;
	private JPasswordField txtPwd = null;
	private JLabel lblUname = null;
	private JLabel lblPwd = null;
	private Border txtborder = BorderFactory.createLineBorder(new Color(157,196,229),2);
	private Image bgimage = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/com/pbs/naresa/images/login2.jpg"));
	private JPanel jPanel = null;
    protected frmContent frmcontent;
	private JLabel lblnew = null;
	
	/**
	 * @throws HeadlessException
	 */
	public frmLogin(){
		// TODO Auto-generated constructor stub
		super();
		initialize();
	}

	



	public void jopt2(){
		Object[] options = {"Yes","No"};
		String mike = "Mike";
		int j = JOptionPane.showOptionDialog(this, "The module is locked.\n Finish the previous module first\n Click yes to exit ", "Press yes to quit ", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE, null, options, options[0]); 
			//JOptionPane.showOptionDialog(this, mike,"Failure Loging in", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE, null, options, options[0]);
		if(j == JOptionPane.YES_OPTION)
			System.exit(-1);
		else{
			System.out.println("in No opt 2");
			
		}
		
		
	}
	
	private JButton getBtnAttach() {
		if (btnAttach == null) {
			btnAttach = new JButton();
			btnAttach.setText("Attach Files");
			btnAttach.setBounds(new Rectangle(444, 320, 168, 24));
			btnAttach.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					String fs = new Character(File.separatorChar).toString();
					String db_path = "db"+fs+"naresa";
					
					JFileChooser fileChooser = new JFileChooser();
					fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			        int returnValue = fileChooser.showOpenDialog(null);
			        if (returnValue == JFileChooser.APPROVE_OPTION) {
			        	File selectedFile = fileChooser.getSelectedFile();
			        	String path = selectedFile.getAbsolutePath();	
			        	if(!path.endsWith(fs))
			        		path += fs;			        	
			        	path+= db_path;
			        	Main.dbpath = path;
			        }
				}
			});
		}
		return btnAttach;
	}

	

	/**
	 * This method initializes btnReset	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnReset() {
		if (btnReset == null) {
			btnReset = new JButton();
			btnReset.setText("Reset");
			btnReset.setBounds(new Rectangle(445, 348, 74, 23));
			btnReset.addActionListener(new java.awt.event.ActionListener() {   
				public void actionPerformed(java.awt.event.ActionEvent e) {    
					txtName.setText("");
					txtPwd.setText("");
				}   
			   
			
			
			});
		}
		return btnReset;
	}

	
	
	/**
	 * This method initializes btnLogin	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnLogin() {
		if (btnLogin == null) {
			btnLogin = new JButton();
			btnLogin.setText("Login");
			btnLogin.setBounds(new Rectangle(533, 348, 78, 23));
			btnLogin.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					//mike
				}
			});
			
				
		}
		return btnLogin;
	}
	
	/**
	 * This method initializes txtName	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	
	private JTextField getTxtName() {
		if (txtName == null) {
			txtName = new JTextField();
			txtName.setName("txtName");
			txtName.setBounds(new Rectangle(444, 267, 166, 23));
			txtName.setBorder(txtborder);
		}
		return txtName;
	}
	

	/**
	 * This method initializes txtPwd	
	 * 	
	 * @return javax.swing.JPasswordField	
	 */
	private JPasswordField getTxtPwd() {
		if (txtPwd == null) {
			txtPwd = new JPasswordField();
			txtPwd.setText("");
			txtPwd.setBounds(new Rectangle(444, 294, 167, 23));
			txtPwd.setBorder(txtborder);
		}
		return txtPwd;
	}
	
	

	/**
	 * This method initializes jPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel() {
		if (jPanel == null) {
			//jPanel = new JPanel();
			jPanel = new JPanel(){
				public void paint(Graphics g){
					g.drawImage(bgimage, 0, 0, this);
				}
			};
			jPanel.setLayout(new GridBagLayout());
			jPanel.setBounds(new Rectangle(0, 0, 996, 708));
			jPanel.setOpaque(false);
		}
		return jPanel;
	}

			
	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(996, 706);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/com/pbs/naresa/images/logoNaresa.jpg")));
		this.setResizable(false);
		this.setForeground(new Color(72, 124, 138));
		this.setBackground(new Color(193, 218, 240));
		this.setContentPane(getJContentPane());
		this.setTitle("Login :: Ministry of health (Naresa)");
		this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		Main.centerWindow(this);
		
		addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
            	if(Main.frmcontent != null){
            		Main.frmcontent.setVisible(true);
                    Main.frmlogin.dispose();
                    Main.frmlogin = null;
                }else{
                    System.exit(1);
                   	 }
            }
        });
		
	}


	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			lblnew = new JLabel();
			lblnew.setBounds(new Rectangle(446, 380, 166, 16));
			lblnew.setForeground(new Color(72, 124, 138));
			lblnew.setHorizontalTextPosition(SwingConstants.CENTER);
			lblnew.setHorizontalAlignment(SwingConstants.LEFT);
			lblnew.setFont(new Font("Dialog", Font.PLAIN, 11));
			lblnew.setText("New User? Register here");
			lblnew.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseEntered(java.awt.event.MouseEvent e) {
					//System.out.println("mouseEntered()"); // TODO Auto-generated Event stub mouseEntered()
					Cursor cursor = lblnew.getCursor();				    
				    lblnew.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

				}
			});
			lblnew.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent e) {
					//useradd = new frmAddUser();					
					//useradd.centerWindow(useradd);
					//useradd.setVisible(true);
					//thisClass.setVisible(false);
					
				}
			});
			lblPwd = new JLabel();
			lblPwd.setHorizontalAlignment(SwingConstants.RIGHT);
			lblPwd.setForeground(new Color(72, 124, 138));
			lblPwd.setBounds(new Rectangle(322, 296, 118, 16));
			lblPwd.setText("Password");
			lblUname = new JLabel();
			lblUname.setHorizontalAlignment(SwingConstants.RIGHT);
			lblUname.setForeground(new Color(72, 124, 138));
			lblUname.setBounds(new Rectangle(308, 270, 130, 16));
			lblUname.setText("User Name");
			jContentPane = new JPanel();
			jContentPane.setLayout(null);
			jContentPane.setBackground(new Color(193, 218, 240));
			jContentPane.add(getBtnAttach(), null);
			jContentPane.add(getBtnReset(), null);
			jContentPane.add(getBtnLogin(), null);
			jContentPane.add(getTxtName(), null);
			jContentPane.add(getTxtPwd(), null);
			jContentPane.add(lblUname, null);
			jContentPane.add(lblPwd, null);
			jContentPane.add(lblnew, null);
			jContentPane.add(getJPanel(), null);
			
			
		}
		return jContentPane;
	}
	
	public void jopt(){
		Object[] options = {"Yes","No"};
		String mike = "Mike";
		int j = JOptionPane.showOptionDialog(this, "No such User\n Quit the application", "Failure Loging in ", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE, null, options, options[0]); 
			//JOptionPane.showOptionDialog(this, mike,"Failure Loging in", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE, null, options, options[0]);
		if(j == JOptionPane.YES_OPTION)
			System.exit(-1);
		else
			btnReset.doClick();
		
		
	}

}  //  @jve:decl-index=0:visual-constraint="-6,9"
